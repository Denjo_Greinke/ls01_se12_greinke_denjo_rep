import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		aufgabe4();
	}
	
	//Gibt Zahlen von 1-100 aus, und �berpr�ft ob sie als Ganzzahl durch 2 teilbar sind.
	public static void aufgabe1() {
		int i = 1;
		while(i < 101) {
			System.out.print(i + " ");
			if(i%2 == 0) {
				System.out.println("ist durch 2 teilbar");
			} else {
				System.out.println("ist nicht durch 2 teilbar");	
			}
			i++;
		}
	}
	
	//Es wird �berpr�ft, ob die Zahlen von 75 bis 100 durch eine vom Nutzer eingegebene Zahl teilbar sind.
	public static void aufgabe2() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Zahl: ");
		int teiler = sc.nextInt();
		
		for(int i=75; i<=150; i++) {
			System.out.print(i + " ");
			if(i%teiler == 0) {
				System.out.println("ist durch "+ teiler +" teilbar");
			} else {
				System.out.println("ist nicht durch "+ teiler +" teilbar");	
			}
		}
	}
	
	public static void aufgabe4() {
		Scanner sc = new Scanner(System.in);
		int ergebnis = 0;
		System.out.println("Zahl: ");
		int zahl = sc.nextInt();
		for(int i=1; i<=zahl; i++) {
			System.out.print(i);
			if(i<zahl) {
				System.out.print(" + ");
			} else if(i==zahl) {
				System.out.print(" = ");
			}
		ergebnis += i;
		}
		
		System.out.println(ergebnis);
	}
	
	
}
