import java.util.Scanner;

public class Teiler {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Zahl:");
		int eingabe = sc.nextInt();
		System.out.println("Teiler von " + eingabe + " sind:");
		
		for(int i=1; i<=eingabe; i++) {
			if(eingabe%i == 0) {
				System.out.println(i);
			}
		}
	}
	
	

}
