import java.util.Scanner;

public class Main {
	static int[] myArray;
	static Scanner sc = new Scanner(System.in);
	//�bungen zu Arrays
	public static void main(String[] args) {
		aufgabe1();
	}

	public static void aufgabe1() {
		//Schritt 1
		myArray = new int[10];
		
		//Schritt 2
		for(int i=0; i<myArray.length; i++) {
			myArray[i] = i*3;
		}
		
		//Schritt 3
		for(int i=0; i<myArray.length; i++) {
			System.out.println(i + ": " + myArray[i]);
		}
		
		//Schritt 4
		myArray[3] = 1000;
		
		//Schritt 5
		System.out.println("\n3: " + myArray[3]);
		
		//Schritt 6
		for(int i=0; i<myArray.length; i++) {
			myArray[i] = 0;
		}
		
		men�();
	}
	
	//Schritt 7
	public static void men�() {
		String input;
		System.out.println("a: Alle Werte ausgeben");
		System.out.println("b: Einen bestimmten Wert ausgeben");
		System.out.println("c: Das Array mit komplett neuen Werten bef�llen");
		System.out.println("d: Einen bestimmten Wert �ndern");
		input = sc.next();
		
		switch(input) {
			case "a":
				for(int i=0; i<myArray.length; i++) {
					System.out.println(i + ": " + myArray[i]);
				}
				break;
			case "b":
				System.out.println("Welcher Wert soll ausgegeben werden?");
				int wert = sc.nextInt();
				System.out.println(wert + ": " + myArray[wert]);
				break;
			case "c":
				for(int i=0; i<myArray.length; i++) {
					System.out.println("Neuer Wert f�r Index " + i + ":");
					wert=sc.nextInt();
					myArray[i] = wert;
				}
				break;
			case "d":
				System.out.println("Welcher Wert soll ge�ndert werden?");
				wert = sc.nextInt();
				System.out.println("Wozu soll der Wert ge�ndert werden?");
				int wert2 = sc.nextInt();
				myArray[wert] = wert2;
				break;
			default:
				System.out.println("Fehler");
				break;
		}
		
		wiederholen();
	}
	
	//Schritt 8
	public static void wiederholen() {
		System.out.println("Soll das Programm wiedeholt werden?");
		String wdh = sc.next();
		if(wdh.equals("y")) {
			men�();
		} else {
			sc.close();
		}
	}
	
}
