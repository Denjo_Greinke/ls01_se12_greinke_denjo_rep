
public class Konsolenausgabe {

	public static void main(String[] args) {
		
		
		//Aufgabe1
		System.out.print("Die Sonne lacht. \n"); //Das ist ein Kommentar
		System.out.println("Hier sind zwei Anführungszeichen: " + "\"\" ");
		
		//Aufgabe2
		System.out.printf("\n %6s \n", "^");
		System.out.printf("%8s \n", "...");
		System.out.printf("%9s \n", ".....");
		System.out.printf("%10s \n", ".......");
		System.out.printf("%11s \n", ".........");
		System.out.printf("%12s \n", "...........");
		System.out.printf("%13s \n", ".............");
		System.out.printf("%8s \n", "...");
		System.out.printf("%8s \n \n", "...");
		
		//Aufgabe 3
		
		System.out.printf("%.2f \n", 22.4234234);
		System.out.printf("%.2f \n", 111.2222);
		System.out.printf("%.2f \n", 4.0);
		System.out.printf("%.2f \n", 1000000.551);
		System.out.printf("%.2f \n", 97.34);
	}

}
