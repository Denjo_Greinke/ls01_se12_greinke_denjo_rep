import java.util.Scanner;

public class Volkshochschule {

	public static void main(String[] args) {
		final int kursgebuehren = 9;
		int endgebuehren;
		double endgebuehrenMitRabatt;
		int stundenzahl;
		int anzahlAbende;
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Stundenzahl pro Abend: ");
		stundenzahl = sc.nextInt();
		System.out.print("Anzahl der Kursabende: ");
		anzahlAbende = sc.nextInt();
		
		endgebuehren = kursgebuehren * stundenzahl * anzahlAbende;
		endgebuehrenMitRabatt = endgebuehren*0.6;
		
		System.out.println("Endgeb�hren: "+endgebuehren);
		System.out.printf("Endgeb�hren mit Sch�lerrabatt: %.2f�", endgebuehrenMitRabatt);
		
	}

}
