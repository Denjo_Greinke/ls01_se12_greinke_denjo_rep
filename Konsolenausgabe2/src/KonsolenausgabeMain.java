/**
 * 
 * @author denjo
 *	�bung 2 zur Formatierung von Konsolenausgaben
 */
public class KonsolenausgabeMain {

	public static void main(String[] args) {
		aufgabe1();
		aufgabe2();
		aufgabe3();
		
	}
	
	public static void aufgabe1() {
		System.out.printf("%4s \n" , "**");
		System.out.print("*");
		System.out.printf("%5s \n", "*");
		System.out.print("*");
		System.out.printf("%5s \n", "*");
		System.out.printf("%4s \n", "**");
		System.out.println("");
	}
	
	public static void aufgabe2() {
		System.out.printf("%-5s", "0!");
		System.out.print("=");
		System.out.printf("%-19s", "");
		System.out.print("=");
		System.out.printf("%4s \n", "1");
		
		System.out.printf("%-5s", "1!");
		System.out.print("=");
		System.out.printf("%-19s", " 1");
		System.out.print("=");
		System.out.printf("%4s \n", "1");
		
		System.out.printf("%-5s", "2!");
		System.out.print("=");
		System.out.printf("%-19s", " 1 * 2");
		System.out.print("=");
		System.out.printf("%4s \n", "2");
		
		System.out.printf("%-5s", "3!");
		System.out.print("=");
		System.out.printf("%-19s", " 1 * 2 * 3");
		System.out.print("=");
		System.out.printf("%4s \n", "6");
		
		System.out.printf("%-5s", "4!");
		System.out.print("=");
		System.out.printf("%-19s", " 1 * 2 * 3 * 4");
		System.out.print("=");
		System.out.printf("%4s \n", "24");
		
		System.out.printf("%-5s", "5!");
		System.out.print("=");
		System.out.printf("%-19s", " 1 * 2 * 3 * 4 * 5");
		System.out.print("=");
		System.out.printf("%4s \n", "120");
		
		System.out.println("");
	}
	
	public static void aufgabe3() {
		System.out.printf("%-12s", "Fahrenheit");
		System.out.print("|");
		System.out.printf("%10s \n", "Celsius");
		
		System.out.println("-----------------------");
		
		System.out.printf("%-12s", "-20");
		System.out.print("|");
		System.out.printf("%10.6s \n", -28.8889);
		
		System.out.printf("%-12s", "-10");
		System.out.print("|");
		System.out.printf("%10.6s \n", -23.3333);
		
		System.out.printf("%-12s", "0");
		System.out.print("|");
		System.out.printf("%10.6s \n", -17.7778);
		
		System.out.printf("%-12s", "+20");
		System.out.print("|");
		System.out.printf("%10.5s \n", -6.6667);
		
		System.out.printf("%-12s", "+30");
		System.out.print("|");
		System.out.printf("%10.5s \n", -1.1111);
	}

}
