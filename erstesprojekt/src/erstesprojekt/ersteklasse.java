package erstesprojekt;

public class ersteklasse {

	public static void main(String[] args) {
		int zahl;		// Deklarierung eines Integers mit dem Namen "zahl".
		double zahl2;	// Deklarierung ...
		boolean bool1;	// Deklarierung ...
		int test1, test2; // Deklarierung zweier Variablen mit gleichem Datentyp
		
		String text1 = "Die Sonne lacht"; // Initialisierung und Deklarierung der Variable "text1"
		zahl = 3;		// Initialisierung der Variable "zahl" mit dem Startwert 3.
		zahl2 = 3.1415926535;	// Initialisierung ...
		bool1 = true;	// Initialisierung ...
		
		System.out.println(text1);
		System.out.println(bool1);
		System.out.print(zahl + "\n");
		System.out.print("zahl2 + zahl = ");
		System.out.print(zahl2 + zahl);
		System.out.println("\n" + "Die Zahlen sind: " + zahl2 + " und " + zahl);
		System.out.printf("Unsere Kommazahl: %.2f", zahl2);
	}

}
