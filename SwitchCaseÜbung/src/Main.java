
public class Main {

	public static void main(String[] args) {
		aufgabe1();

	}
	
	
	//Wenn bei einem case der break fehlt, wird auch der n�chste case ausgef�hrt.
	public static void aufgabe1() {
		int i=0;
		
		switch(i) {
		case 0 :
			System.out.println("Hier fehlt ein break.");
		case 1 :
			System.out.println("Denn i ist nicht 1");
			break;
		}
	}

}
