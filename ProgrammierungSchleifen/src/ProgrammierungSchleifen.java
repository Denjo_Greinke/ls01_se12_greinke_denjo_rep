import java.util.Scanner;

/**
 * Programm zum AB Programmierung_Schleifen_2
 * @author Denjo
 *
 */

public class ProgrammierungSchleifen {

	public static void main(String[] args) {
		aufgabe2();
	}
	
	public static void aufgabe1() {
		int eingabe1, eingabe2;
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Anfangszahl: ");
		eingabe1 = sc.nextInt();
		System.out.println("Endzahl: ");
		eingabe2 = sc.nextInt();
		sc.close();
		
		for(int i=eingabe1; i<eingabe2; i++) {
			System.out.println(i);
		}
	}
	
	public static void aufgabe2() {
		int laufzeit = 20;
		double zinssatz = 4.2;
		double anlage = 10000.0;
		double endkapital = 0.0;
		Scanner sc = new Scanner(System.in);
		
		System.out.printf("Laufzeit (in Jahren) des Sparvertrags%.13s: " + laufzeit + "\n", "...................................");
		System.out.printf("Wie viel Kapital (in Euro) m�chten Sie anlegen?%.3s: ", "...................................");
		anlage = sc.nextDouble();
		sc.close();
		System.out.printf("Zinssatz (in Prozent)%.29s: " + zinssatz + "\n", ".........................................");
		
		for(int i=1; i<laufzeit+1; i++) {
			if(i==1) {
				endkapital = anlage + anlage*(zinssatz/100);
			} else {
				endkapital = endkapital + endkapital*(zinssatz/100);
			}
		}
		endkapital = runden(endkapital, 2);
		
		System.out.println("\nEingezahltes Kapital: " + anlage + "�");
		System.out.println("Ausgezahltes Kapital: " + endkapital + "�");
	}
	
	public static double runden(double wert, int nachkommaStellen) {
        double d = Math.pow(10, nachkommaStellen);
        return Math.round(wert * d) / d;
    }

}
