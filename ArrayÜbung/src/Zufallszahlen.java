import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Zufallszahlen {
	static int[] liste = new int[20];
	
	public static void main(String[] args) {
		rnd();
		ausgabe();
	}
	
	public static void rnd() {
		for(int i=0; i<liste.length; i++) {
			int rnd = ThreadLocalRandom.current().nextInt(0, 11);
			liste[i] = rnd;
		}
	}
	
	public static void ausgabe() {
		for(int i=0; i<liste.length; i++) {
			System.out.println(liste[i]);
		}
	}
}
