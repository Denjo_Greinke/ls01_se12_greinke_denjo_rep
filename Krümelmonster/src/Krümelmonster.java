import java.util.Scanner;

public class Krümelmonster {

public static void main(String[] args) {
		
		int kekse = 100;
		int anzahl;
		Scanner tastatur = new Scanner(System.in);
		
		Krümelmonster.sprich();
			
		kekse = Krümelmonster.friss5(kekse);
		System.out.println("Anzahl Kekse nach dem Fressen von 5 Keksen: " + kekse);
		
		kekse = Krümelmonster.bring10(kekse);
		System.out.println("Anzahl Kekse nach dem Bringen von 10 Keksen: " + kekse);

		System.out.println("Wie viele Kekse bringt der Nikolaus? ");
		anzahl = tastatur.nextInt();	
		
		kekse = Krümelmonster.nikolaus(kekse, anzahl);
		System.out.println("Anzahl Kekse nach dem Nikolaus: " + kekse);
		
		//kekse = Krümelmonster.frissAlleKekse();
		//System.out.println("Anzahl Kekse nach dem das Krümelmonster alle gegessen hat: " + kekse);
		
		kekse = Krümelmonster.verschenkeDieHaelfte(kekse);
	}
	
	
	public static void sprich() {
		System.out.println("Ich will Kekseeeee!!!");
	}
	
	public static int friss5(int a) {
		a = a-5;
		return a;
	}
	
	public static int bring10(int b) {
		b = b+10;
		return b;
	}
	
	public static int nikolaus(int kekse, int anzahl) {
		kekse = kekse + anzahl;
		return kekse;
	}
	
	public static int frissAlleKekse() {
		return 0;
	}
	
	/**
	 * Ist die Anzahl an Keksen ungerade, verschenkt das Krümelmonster einen halben Keks mehr.
	 * @param anzahlKekse
	 * @return
	 */
	public static int verschenkeDieHaelfte(int anzahlKekse) {
		if(anzahlKekse == 0) {
			System.out.println("Das Krümelmonster hat keine Kekse die es verschenken kann.");
			return 0;
		} else {
			anzahlKekse = anzahlKekse/2;
			System.out.println("Das Krümelmonster hat die Hälfte seiner Kekse verschenkt und hat noch " + anzahlKekse + " Kekse.");
			return anzahlKekse;
		}
	}
	
}
