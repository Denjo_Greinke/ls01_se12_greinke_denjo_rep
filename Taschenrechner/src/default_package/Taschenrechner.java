package default_package;

import java.util.Scanner;

/**
 * 
 * @author Denjo Greinke
 * �bung zum Rechnen mit Variablen
 */
public class Taschenrechner {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int eingabe1;
		int eingabe2;
		String operator;
		double pi = 3.1415;
		int radius = 2;
		double ergebnis;
		double ergebnis2;
		double ergebnis3;
		
		ergebnis = 2*pi*radius;
		ergebnis2 = 1+radius/(double)5;
		
		System.out.printf("2*pi*radius %2s \n", "=");
		System.out.printf("2*"+pi+"*"+radius+" %3s " + "%6.1f \n", "=", ergebnis);
		System.out.printf("\n1 + ("+radius+" / 5)"+ "%3s"+"%6.1f \n", "=", ergebnis2);
		
		System.out.println("\nGib eine Rechnung ein:");
		System.out.println("Wert 1:");
		eingabe1 = scanner.nextInt();
		System.out.println("Rechenoperator:");
		operator = scanner.next();
		System.out.println("Wert 2:");
		eingabe2 = scanner.nextInt();
		ergebnis3 = berechnen(operator, eingabe1, eingabe2);
		System.out.println(eingabe1+ operator +eingabe2+" = "+ ergebnis3);
	}
	
	public static double berechnen(String pOperator, int pWert1, int pWert2) {
		double ergebnis;
		System.out.println(pOperator);
		if(pOperator.contains("+")) {
			ergebnis = pWert1 + pWert2;
		} else if(pOperator.contains("-")) {
			ergebnis = pWert1 - pWert2;
		} else if(pOperator.contains("*")) {
			ergebnis = pWert1 * pWert2;
		} else if(pOperator.contains("/")) {
			ergebnis = pWert1 / pWert2;
		} else {
			return 0;
		}
		return ergebnis;
	}

}
