import java.util.Scanner;

public class DivideBy5 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Zahl: ");
		int eingabe = sc.nextInt();
		
		System.out.println("Die Zahl " + eingabe + " ist " + teilenDurchFünf(eingabe) + " mal durch 5 teilbar");
	}
	
	public static int teilenDurchFünf(int pEingabe) {
		int i=0;
		
		while(pEingabe%5 == 0) {
			pEingabe = pEingabe/5;
			i++;
		}
		return i;
	}

}
