/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author Denjo Greinke
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten =  8;
    
    // Anzahl der Sterne in unserer Milchstra�e
    long anzahlSterne = 100000000000l;
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3645000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    int alterTage = 7198;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm = 150000;
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
    int flaecheGroessteLand = 17100000;
    
    // Wie gro� ist das kleinste Land der Erde?
    
    double flaecheKleinsteLand = 0.44;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne in der Milchstra�e: " + anzahlSterne);
    
    System.out.println("Berlin hat " + bewohnerBerlin + " Einwohner");
    
    System.out.println("Ich bin " + alterTage + " Tage alt");
    
    System.out.println("Das schwerste Tier der Welt wiegt " + gewichtKilogramm + "kg");
    
    System.out.println("Das Fl�che des gr��ten Landes betr�gt " + flaecheGroessteLand + "km^2");
    
    System.out.println("Die Fl�che des kleinsten Landes betr�gt " + flaecheKleinsteLand + "km^2");
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}
