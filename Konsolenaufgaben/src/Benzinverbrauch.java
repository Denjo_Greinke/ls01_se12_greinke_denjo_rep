import java.util.Scanner;

public class Benzinverbrauch {

	public static void main(String[] args) {
		int kmStand1;
		int kmStand2;
		double benzinVerbrauch;
		int difKmStand;
		double verbrauch100km;
		Scanner sc = new Scanner(System.in);
		
		System.out.print("km-stand bei letzten Tanken: ");
		kmStand1 = sc.nextInt();
		System.out.print("km-stand beim Tanken.......: ");
		kmStand2 = sc.nextInt();
		System.out.print("Benzinverbrauch............: ");
		benzinVerbrauch = sc.nextDouble();
		
		difKmStand = kmStand2/100 - kmStand1/100;
		verbrauch100km = benzinVerbrauch/difKmStand;
		
		System.out.printf("Der PKW hat %.2f Liter auf 100km verbraucht.", verbrauch100km);
	}

}
