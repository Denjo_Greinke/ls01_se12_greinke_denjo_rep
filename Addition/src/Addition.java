import java.util.Scanner;

class Addition {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        double zahl1 = 0.0, zahl2 = 0.0, erg = 0.0;

        //1.Programmhinweis
        Addition.druckenHinweis();

        //4.Eingabe
        Addition.eingabe(); // R�CKGABE

        //3.Verarbeitung
        erg = zahl1 + zahl2;

        //2.Ausgabe
        System.out.println("Ergebnis der Addition");
        System.out.printf("%.2f = %.2f+%.2f", erg, zahl1, zahl2);
    }
    
    public static void druckenHinweis() {
    	System.out.println("Hinweis: ");
        System.out.println("Das Programm addiert 2 eingegebene Zahlen. ");
    }
    
    public static double[] eingabe() {
    	System.out.println(" 1. Zahl:");
        double zahl1 = sc.nextDouble();
        System.out.println(" 2. Zahl:");
        double zahl2 = sc.nextDouble(); //wie beide Werte zur�ckgeben? Array?
        double[] zahlen = {zahl1, zahl2};
        
		return zahlen;
    }
}