import java.util.Scanner;

public class Bruttolohn {

	public static void main(String[] args) {
		double arbeitszeit;
		double stundenlohn;
		double monatslohn;
		double jahreslohn;
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Arbeitszeit im Monat (h): ");
		arbeitszeit = sc.nextDouble();
		System.out.print("Stundenlohn             : ");
		stundenlohn = sc.nextDouble();
		
		monatslohn = arbeitszeit * stundenlohn;
		jahreslohn = monatslohn * 12.0;
		
		System.out.printf("Der Monatslohn betr�gt %.2f Euro\n", monatslohn);
		System.out.printf("Der Jahreslohn betr�gt %.2f Euro", jahreslohn);
	}

}
