import java.util.Scanner;

public class Investor {

	public static void main(String[] args) {
		double einzahlung;
		double zinsen;
		int jahre;
		double zinsenzahlung;
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Einzahlung in Euro: ");
		einzahlung = sc.nextDouble();
		System.out.print("Zinsen in Prozent : ");
		zinsen = sc.nextDouble();
		System.out.print("Jahre             : ");
		jahre = sc.nextInt();
		zinsenzahlung = einzahlung * zinsen/100 * jahre;
		
		System.out.printf("Der Investor hat insgesamt %.2f Euro erhalten", zinsenzahlung);
	}

}
