import java.util.Random;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		int[] array = new int[10];
		int[] rnd = new int[100];
		Scanner sc = new Scanner(System.in);
		
		for(int i=0; i<array.length; i++) {
			System.out.println("Wert f�r Index " + i + ": ");
			array[i] = sc.nextInt();
		}
		
		for(int i=0; i<rnd.length; i++) {
			Random gen = new Random();
			rnd[i] = gen.nextInt(1000);
		}
		
		for(int i=0; i<rnd.length; i++) {
			if(rnd[i]%3 == 0) {
				System.out.println("Der Wert " + rnd[i] + " an Index " + i + " ist durch 3 teilbar.");
			}
		}
	}

}
