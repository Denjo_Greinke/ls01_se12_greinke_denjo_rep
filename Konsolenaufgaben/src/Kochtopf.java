import java.util.Scanner;

public class Kochtopf {

	public static void main(String[] args) {
		final double pi = 3.141593;
		double durchmesser;
		double radius;
		double hoehe;
		double volumen;
		Scanner sc = new Scanner(System.in);
		
		System.out.print("H�he des Kochtopfes        (cm):");
		hoehe = sc.nextDouble();
		System.out.print("Durchmesser des Kochtopfes (cm):");
		durchmesser = sc.nextDouble();
		radius = durchmesser/2.0;
		volumen = pi * radius * radius * hoehe / 1000;
		
		System.out.printf("Das Volumen des Kochtopfes betr�gt %.2f Liter", volumen);
	}

}
