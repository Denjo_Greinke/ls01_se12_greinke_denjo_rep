//Nur Aufgabe 3, bearbeitet von Denjo Greinke
public class Temperaturtabelle {

	public static void main(String[] args) {
		System.out.printf("%-12s", "Fahrenheit");
		System.out.print("|");
		System.out.printf("%10s \n", "Celsius");
		
		System.out.println("-----------------------");
		
		System.out.printf("%-12s", "-20");
		System.out.print("|");
		System.out.printf("%10.6s \n", -28.8889);
		
		System.out.printf("%-12s", "-10");
		System.out.print("|");
		System.out.printf("%10.6s \n", -23.3333);
		
		System.out.printf("%-12s", "0");
		System.out.print("|");
		System.out.printf("%10.6s \n", -17.7778);
		
		System.out.printf("%-12s", "+20");
		System.out.print("|");
		System.out.printf("%10.5s \n", -6.6667);
		
		System.out.printf("%-12s", "+30");
		System.out.print("|");
		System.out.printf("%10.5s \n", -1.1111);
	}

}
