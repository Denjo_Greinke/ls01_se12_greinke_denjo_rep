import java.util.Random;
import java.util.Scanner;

public class Verschieben {
	static int[] array = new int[5];
	
	public static void main(String[] args) {
		for(int i=0; i<array.length; i++) {
			Random rnd = new Random();
			array[i]= rnd.nextInt(100);
		}
		ausgeben();
		verschieben();
		ausgeben();
		
	}
	
	public static void ausgeben() {
		for(int i=0; i<array.length; i++) {
			System.out.print(array[i]+ ", ");
		}
		System.out.println();
	}
	
	public static void verschieben() {
		Scanner sc = new Scanner(System.in);
		int neu = sc.nextInt();
		
		for(int i=array.length-1; i>0; i--) {
			array[i] = array[i-1];
		}
		
		array[0] = neu;
		sc.close();
	}

}
