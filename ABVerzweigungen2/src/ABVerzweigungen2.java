import java.util.Scanner;

public class ABVerzweigungen2 {

	public static void main(String[] args) {
		gewichtsberechnung();
	}
	
	//Aufgabe 2
	public static void geburtsquartal() {
		int monat;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("In welchem Monat (1-12) sind Sie geboren?");
		monat = sc.nextInt();
		
		System.out.println("Sie sind im ");
		if(monat <= 3) {
			System.out.println("I. Quartal geboren.");
		} else if(monat <= 6) {
			System.out.println("II. Quartal geboren.");
		} else if(monat <= 9) {
			System.out.println("III. Quartal geboren.");
		} else {
			System.out.println("IV. Quartal geboren.");
		}
		
	}
	
	//Aufgabe 3
	public static void computerladen() {
		int stückzahl, versandkosten = 15;
		double preis = 0.0;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Wie viele Rohlinge möchten Sie bestellen?");
		stückzahl = sc.nextInt();
		
		if(stückzahl >0 && stückzahl <=9) {
			preis = stückzahl * 1.1;
		} else if (stückzahl >9 && stückzahl <=49) {
			preis = stückzahl * 0.95;
		} else if (stückzahl > 49) {
			preis = stückzahl * 0.85;
		}
		
		System.out.print("Der Preis beträgt: ");
		if(preis < 60.0) {
			preis = preis + versandkosten;
		}
		System.out.printf("%.2f€", preis);
		
	}
	
	//Aufgabe 4
	public static void bußgeldkatalog() {
		int gefahren, erlaubt, überschreitung;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Was ist die gefahrene geschwindigkeit? ");
		gefahren = sc.nextInt();
		System.out.println("Was ist die erlaubte geschwindigkeit? (20km/h, 30km/h, 50km/h)");
		erlaubt = sc.nextInt();
		
		überschreitung = gefahren - erlaubt;
		
		if(überschreitung < 1) {
			System.out.println("Es gab keine Überschreitung der Höchstgeschwindigkeit.");
		} else if(überschreitung > 0) {
			System.out.print("Es gab eine Überschreitung von " + überschreitung + "km/h. Laut Bußgeldkatalog gibt es ");
			
			if(überschreitung < 21 && überschreitung > 0) {
				System.out.println("keine Punkte, nur Bußgeld.");
			} else if(überschreitung < 26) {
				System.out.println("einen Punkt.");
			} else if(überschreitung < 41) {
				System.out.println("drei Punkte.");
			} else if(überschreitung > 40) {
				System.out.println("vier Punkte.");
			}
		}
		
		
	}
	
	//Aufgabe 5
	public static void gewichtsberechnung() {
		double normalgewicht, idealgewicht = 0.0;
		int körperlänge;
		char geschlecht;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Körperlänge in cm: ");
		körperlänge = sc.nextInt();
		System.out.println("Geschlecht: ");
		geschlecht = sc.next().toCharArray()[0];
		
		normalgewicht = körperlänge - 100;
		if(geschlecht == 'm' || geschlecht == 'M') {
			idealgewicht = normalgewicht - (normalgewicht*0.1);
		} else if(geschlecht == 'w' || geschlecht == 'W') {
			idealgewicht = normalgewicht - (normalgewicht*0.15);
		}
		
		System.out.printf("Das Normalgewicht beträgt %.2fkg\n", normalgewicht);
		System.out.printf("Das Idealgewicht beträgt %.2fkg\n", idealgewicht);
	}

}
