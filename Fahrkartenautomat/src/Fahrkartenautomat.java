﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static Fahrkarte[] alleFahrkarten;
	public static Scanner tastatur = new Scanner(System.in);
	
    public static void main(String[] args) {
    	while(true) {
    		bestellvorgang();
    	}
    }
    
    public static void bestellvorgang() {
        
        double zuZahlenderBetrag; 
        double eingezahlterGesamtbetrag;
        double rückgabebetrag;

        zuZahlenderBetrag = Fahrkartenautomat.fahrkartenbestellungErfassen(); //Bestellen der Fahrkarte(n)
        if(zuZahlenderBetrag == 0) {
        	System.out.println("\n-----------------------------\n");
        	return;
        }
        eingezahlterGesamtbetrag = Fahrkartenautomat.fahrkartenBezahlen(zuZahlenderBetrag); //Geldeinwurf
        Fahrkartenautomat.fahrkartenAusgeben(); //Fahrscheinausgabe
        rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag; //Rückgeldberechnung und -Ausgabe
        rückgabebetrag = runden(rückgabebetrag, 2);
        Fahrkartenautomat.rueckgeldAusgeben(rückgabebetrag);
    }
    
    
    
    public static double fahrkartenbestellungErfassen() {
    	double zuZahlenderBetrag;
    	int fahrkartenNummer;
    	byte anzahlTickets = 0;
    	
    	System.out.println("Welches Ticket möchten Sie kaufen? ");
    	ausgebenFahrkartenArten();
    	fahrkartenNummer = tastatur.nextInt();
    	
    	if(fahrkartenNummer > 0 && fahrkartenNummer < 11) {
    		System.out.println("Anzahl der Tickets (1-10): ");
            anzahlTickets = tastatur.nextByte();
            if(anzahlTickets < 0 || anzahlTickets >10) {
            	System.out.println("Fehleingabe");
            	Fahrkartenautomat.fahrkartenbestellungErfassen();
            }
            
            alleFahrkarten = new Fahrkarte[anzahlTickets];
            
    		for(int i=0; i<anzahlTickets; i++) {
    			Fahrkarte fahrkarte = new Fahrkarte(fahrkartenNummer);
    			alleFahrkarten[i] = fahrkarte;
    		}
    		
    	} else {
    		System.out.println("Fehleingabe");
    		Fahrkartenautomat.fahrkartenbestellungErfassen();
    	}
    	
        zuZahlenderBetrag = alleFahrkarten[0].getPreis();
        zuZahlenderBetrag = runden(zuZahlenderBetrag, 2);
        zuZahlenderBetrag = zuZahlenderBetrag*anzahlTickets;
        zuZahlenderBetrag = runden(zuZahlenderBetrag, 2);
        
        return zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	double eingezahlterGesamtbetrag = 0.0;
    	double eingeworfeneMünze;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
        	double d = zuZahlenderBetrag - eingezahlterGesamtbetrag;
     	    d = runden(d, 2);
     	    System.out.printf("Noch zu zahlen: %.2f Euro \n", d);
     	    System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	    eingeworfeneMünze = tastatur.nextFloat();
     	    eingeworfeneMünze = runden(eingeworfeneMünze, 2);
     	    if(eingeworfeneMünze != 0.05 && eingeworfeneMünze != 0.1 && eingeworfeneMünze != 0.2 && eingeworfeneMünze != 0.5 && eingeworfeneMünze != 1.0 && eingeworfeneMünze != 2.0) {
     	    	System.out.println("Fehler: Falsche Münzeingabe");
     	    	eingeworfeneMünze = 0.0;
     	    }
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        return eingezahlterGesamtbetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
        
        for (int i = 0; i < 26; i++) {
        	System.out.print("=");
        	warte(65);
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rückgabebetrag) {
    	if(rückgabebetrag > 0.0) {
    		System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO \n", rückgabebetrag);
     	    System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) {// 2 EURO-Münzen 
            	münzeAusgeben(2, "EURO");
 	            rückgabebetrag -= 2.0;
 	            rückgabebetrag = runden(rückgabebetrag, 2);
            }
            while(rückgabebetrag >= 1.0) {// 1 EURO-Münzen
            	münzeAusgeben(1, "EURO");
 	            rückgabebetrag -= 1.0;
 	            rückgabebetrag = runden(rückgabebetrag, 2);
            }
            while(rückgabebetrag >= 0.5) {// 50 CENT-Münzen
            	münzeAusgeben(50, "CENT");
 	            rückgabebetrag -= 0.5;
 	            rückgabebetrag = runden(rückgabebetrag, 2);
            }
            while(rückgabebetrag >= 0.2) {// 20 CENT-Münzen
            	münzeAusgeben(20, "CENT");
  	            rückgabebetrag -= 0.2;
  	            rückgabebetrag = runden(rückgabebetrag, 2);
            }
            while(rückgabebetrag >= 0.1) {// 10 CENT-Münzen
            	münzeAusgeben(10, "CENT");
 	            rückgabebetrag -= 0.1;
 	            rückgabebetrag = runden(rückgabebetrag, 2);
            }
            while(rückgabebetrag >= 0.05) {// 5 CENT-Münzen
            	münzeAusgeben(5, "CENT");
  	            rückgabebetrag -= 0.05;
  	            rückgabebetrag = runden(rückgabebetrag, 2);
            }
        }
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+"vor Fahrtantritt entwerten zu lassen!\n"+"Wir wünschen Ihnen eine gute Fahrt.");
        System.out.println("\n-----------------------------\n");
    }
    
    /**
     * rundet Zahlen auf eine selbstgewählte Zahl an Nachkommastellen
     */
    public static double runden(double wert, int nachkommaStellen) {
        double d = Math.pow(10, nachkommaStellen);
        return Math.round(wert * d) / d;
    }
    
    public static void warte(int millis) {
    	try {
    		Thread.sleep(millis);
    	} catch (InterruptedException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    }
    
    public static void ausgebenFahrkartenArten() {
    	for(int i=1; i<11; i++) {
    		Fahrkarte temp = new Fahrkarte(i);
    		System.out.println(i + ": " + temp.getBezeichnung());
    	}
    }
    
    public static void münzeAusgeben(int betrag, String einheit) {
    	System.out.println(betrag + " " + einheit);
    }
    
}